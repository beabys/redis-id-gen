.PHONY: all
all: help

SHELL:=/bin/bash

include ./configs/.env
export $(shell sed 's/=.*//' ./configs/.env)

.PHONY: help
help:
	############################################################################
	#
	# available commands:
	#
	# up            - run docker-compose up and tail the logs
	# upd           - run docker-compose up (run up the container)
	# down          - run docker-compose down (shutdown the container)
	# run           - run locally the app 
	# test          - runt test
	# test-cover    - run test and create coverage files
	# help          - show this dialog 
	#
	############################################################################

.PHONY: up
up: upd logs

.PHONY: upd
upd: 
	docker-compose -f build/docker-compose.yaml up --build -d

.PHONY: down
down:
	docker-compose -f build/docker-compose.yaml down

.PHONY: logs
logs:
	docker-compose -f build/docker-compose.yaml logs -t -f --tail=10

.PHONY: run
run:
	go mod vendor && go run -mod=vendor cmd/main.go

.PHONY: test
test:
	go mod vendor && go test -mod=vendor -v ./... -coverprofile .testCoverage.txt 

.PHONY: cover-html
cover-html:
	go tool cover -html=.testCoverage.txt -o unit.html

.PHONY: test-cover
test-cover: test cover-html
