# redis-id-gen

Simple application to get redis keys and used like id's

## Routes available

- "/"  return only one id
- "/{number of id}" will return the number of id's requested

## Command available

    make up - run docker-compose up and tail the logs

	make upd - run docker-compose up (run up the container)

	make down - run docker-compose down (shutdown the container)

	make run - run locally the app

## Requirements

- GO 1.14+ 

- GO111MODULE=on

- Docker and Docker Compose

author: @beabys