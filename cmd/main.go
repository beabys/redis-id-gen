package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"

	"github.com/go-redis/redis"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/prometheus/common/log"
	"gitlab.com/beabys/redis-id-gen/internal/utils"
	"gitlab.com/beabys/redis-id-gen/pkg/config"
)

// Application type
type Application struct {
	Config   *config.Config
	Redis    *redis.Client
	Response *Response
}

// Response type
type Response struct {
	StatusCode int
	Content    *ResponseContent
}

// ResponseContent type
type ResponseContent struct {
	Code int         `json:"code"`
	Msg  interface{} `json:"msg"`
	Data interface{} `json:"data"`
}

func main() {
	config := config.LoadConfig()
	redis := utils.GetRedis(&config)
	app := Application{
		Config: &config,
		Redis:  redis,
	}
	r := mux.NewRouter()
	r.Use(loggingMiddleware)
	r.NotFoundHandler = http.HandlerFunc(app.notFound)
	r.HandleFunc("", app.getOne).Methods("GET")
	r.HandleFunc("/", app.getOne).Methods("GET")
	r.HandleFunc("/{ids}", app.getMulti).Methods("GET")
	log.Infof("Starting Server on port: %v", app.Config.App.Port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%v", app.Config.App.Port), r))
}

func loggingMiddleware(next http.Handler) http.Handler {
	return handlers.CombinedLoggingHandler(os.Stdout, next)
}

// not found handler middleware
func (app *Application) notFound(w http.ResponseWriter, r *http.Request) {
	content := ResponseContent{404, "Not Found", ""}
	response := Response{404, &content}
	app.Response = &response
	app.responseWritter(w)
	return
}

func (app *Application) getOne(w http.ResponseWriter, r *http.Request) {
	content := ResponseContent{0, nil, ""}
	response := Response{200, &content}
	app.Response = &response
	v := app.getIDs(1)
	if len(v) < 1 {
		app.Response.StatusCode = 500
		app.Response.Content.Code = 500
		app.Response.Content.Msg = "Something went wrong ... :("
		app.responseWritter(w)
		return
	}
	app.Response.Content.Data = v
	app.responseWritter(w)
}

func (app *Application) getMulti(w http.ResponseWriter, r *http.Request) {
	content := ResponseContent{200, nil, ""}
	response := Response{200, &content}
	app.Response = &response
	vars := mux.Vars(r)
	ids, err := strconv.ParseInt(vars["ids"], 10, 64)
	if err != nil {
		app.Response.StatusCode = 400
		app.Response.Content.Code = 400
		app.Response.Content.Msg = fmt.Sprintf("Impossible transform %v into integer", vars["ids"])
		app.responseWritter(w)
		return
	}
	if ids < 1 {
		app.Response.StatusCode = 400
		app.Response.Content.Code = 400
		app.Response.Content.Msg = "Invalid range of id's"
		app.responseWritter(w)
		return
	}
	v := app.getIDs(ids)
	if len(v) < 1 {
		app.Response.StatusCode = 500
		app.Response.Content.Code = 500
		app.Response.Content.Msg = "Something went wrong ... :("
		app.responseWritter(w)
		return
	}
	app.Response.Content.Data = v
	app.responseWritter(w)
}

func (app *Application) getIDs(n int64) []int64 {
	var result []int64
	key := app.Config.Redis.Key
	redis := app.Redis
	initialKey := redis.Get(key).Val()
	if initialKey == "" {
		redis.Incr(key)
	}
	v := redis.IncrBy(key, n).Val()
	for r := v - n; r < v; r++ {
		result = append(result, r)
	}
	return result
}

func (app *Application) responseWritter(w http.ResponseWriter) {
	w.WriteHeader(app.Response.StatusCode)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(app.Response.Content)
}
