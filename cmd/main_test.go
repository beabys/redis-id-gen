package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/alicebob/miniredis"
	"github.com/elliotchance/redismock"
	"github.com/go-redis/redis"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gitlab.com/beabys/redis-id-gen/pkg/config"
)

var (
	val = "val"
)

func getMockConfiguration() *config.Config {
	var fakeRedisConfig config.RedisConfig
	var fakeAppConfig config.ApplicationConfig
	fakeAppConfig.Port = 8080
	fakeRedisConfig.Key = "test"
	configuration := config.Config{fakeAppConfig, fakeRedisConfig}
	return &configuration
}

func mockRedis() *miniredis.Miniredis {
	redis, err := miniredis.Run()
	if err != nil {
		panic(err)
	}
	return redis
}

func redisClient() *redis.Client {
	redisServer := mockRedis()
	redisClient := redis.NewClient(&redis.Options{
		Addr: redisServer.Addr(),
	})
	return redisClient
}

func setApp() *Application {
	app := &Application{
		Config: getMockConfiguration(),
		Redis:  redisClient(),
	}
	return app
}

// execute Test Application
func TestApplication(t *testing.T) {
	// set App
	app := setApp()
	t.Run("test not found page", func(t *testing.T) {
		notFoundTest(t, app)
	})
	t.Run("Return Success one id", func(t *testing.T) {
		successIds(t, app, 1)
	})
	t.Run("Return Success multiple", func(t *testing.T) {
		successIds(t, app, 5)
	})
	t.Run("Return badRequest Response for negative ids", func(t *testing.T) {
		testMultipleIds(t, app, "-2", true, false)
	})
	t.Run("Return badRequest Response for empty ids", func(t *testing.T) {
		testMultipleIds(t, app, "2", false, true)
	})
	t.Run("Return Success Response for 2 ids", func(t *testing.T) {
		testMultipleIds(t, app, "2", false, false)
	})
	t.Run("Return one success Response", func(t *testing.T) {
		testOneIds(t, app, "1", false, false)
	})
}

func notFoundTest(t *testing.T, app *Application) {
	recorder := httptest.NewRecorder()
	handler := http.HandlerFunc(app.notFound)
	req, err := http.NewRequest("GET", "/noexist/10000", nil)
	if err != nil {
		t.Fatal(err)
	}
	handler.ServeHTTP(recorder, req)
	// Check the status code is what we expect.
	status := recorder.Code
	assert.NoError(t, err)
	assert.Equal(t, http.StatusNotFound, status)
}

func successIds(t *testing.T, app *Application, expected int) {
	client := app.Redis
	mock := redismock.NewNiceMock(client)
	key := app.Config.Redis.Key
	mock.On("Get", key).Return(redis.NewStringResult(val, nil))
	n := int64(expected)
	result := app.getIDs(n)
	received := len(result)

	assert.Equal(t, expected, received)
}

func testMultipleIds(t *testing.T, app *Application, elements string, negative bool, emptyVars bool) {
	statusExpected := http.StatusOK
	client := app.Redis
	mock := redismock.NewNiceMock(client)
	key := app.Config.Redis.Key
	mock.On("Get", key).Return(redis.NewStringResult(val, nil))
	req, err := http.NewRequest("GET", "/2", nil)
	vars := map[string]string{
		"ids": elements,
	}
	if err != nil {
		t.Fatal(err)
	}
	if emptyVars {
		vars = map[string]string{}
		statusExpected = http.StatusBadRequest
	}
	if negative {
		statusExpected = http.StatusBadRequest
	}
	req = mux.SetURLVars(req, vars)
	w := httptest.NewRecorder()
	app.getMulti(w, req)
	status := w.Code
	assert.NoError(t, err)
	assert.Equal(t, statusExpected, status)
}

func testOneIds(t *testing.T, app *Application, elements string, negative bool, emptyVars bool) {
	statusExpected := http.StatusOK
	client := app.Redis
	mock := redismock.NewNiceMock(client)
	key := app.Config.Redis.Key
	mock.On("Get", key).Return(redis.NewStringResult(val, nil))
	req, err := http.NewRequest("GET", "/2", nil)
	vars := map[string]string{
		"ids": elements,
	}
	if err != nil {
		t.Fatal(err)
	}
	if emptyVars {
		vars = map[string]string{}
		statusExpected = http.StatusBadRequest
	}
	if negative {
		statusExpected = http.StatusBadRequest
	}
	req = mux.SetURLVars(req, vars)
	w := httptest.NewRecorder()
	app.getOne(w, req)
	status := w.Code
	assert.NoError(t, err)
	assert.Equal(t, statusExpected, status)
}
