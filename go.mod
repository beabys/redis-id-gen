module gitlab.com/beabys/redis-id-gen

go 1.15

require (
	github.com/alicebob/gopher-json v0.0.0-20200520072559-a9ecdc9d1d3a // indirect
	github.com/alicebob/miniredis v2.5.0+incompatible
	github.com/elliotchance/redismock v1.5.3
	github.com/fatih/color v1.9.0
	github.com/go-redis/redis v6.15.8+incompatible
	github.com/gomodule/redigo v1.8.2 // indirect
	github.com/gorilla/handlers v1.4.2
	github.com/gorilla/mux v1.7.4
	github.com/onsi/ginkgo v1.14.0 // indirect
	github.com/prometheus/common v0.10.0
	github.com/spf13/viper v1.7.0
	github.com/stretchr/testify v1.5.1
	github.com/yuin/gopher-lua v0.0.0-20200816102855-ee81675732da // indirect
)
