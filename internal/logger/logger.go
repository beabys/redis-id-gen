package logger

import (
	"log"

	"github.com/fatih/color"
)

// Debug level log
func Debug(format string, v ...interface{}) {
	log.Printf(format, v...)
}

// Info level log
func Info(format string, v ...interface{}) {
	log.Printf(color.GreenString(format, v...))
}

// Warn level
func Warn(format string, v ...interface{}) {
	log.Printf(color.YellowString(format, v...))
}

// Error level
func Error(format string, v ...interface{}) {
	log.Printf(color.RedString(format, v...))
}

// Fatal level log, will terminate the program
func Fatal(v ...interface{}) {
	log.Fatal(color.New(color.FgWhite).Add(color.BgRed).Sprint(v...))
}
