package utils

import (
	"fmt"

	"github.com/go-redis/redis"
	"gitlab.com/beabys/redis-id-gen/internal/logger"
	"gitlab.com/beabys/redis-id-gen/pkg/config"
)

// GetRedis returns the new pool connection of redis
func GetRedis(c *config.Config) *redis.Client {
	// var address =
	var redisConfig = &redis.Options{
		Addr: fmt.Sprintf("%v:%v", c.Redis.Host, c.Redis.Port),
		DB:   c.Redis.DB,
	}
	if c.Redis.Password != "" {
		redisConfig.Password = c.Redis.Password
	}

	client := redis.NewClient(redisConfig)

	//check redis connection
	_, err := client.Ping().Result()
	if err != nil {
		logger.Fatal(err)
	}

	return client
}
