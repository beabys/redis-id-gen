package config

import (
	"log"

	"github.com/spf13/viper"
)

// Config is a struct define configuration for the app and list of DSP
type Config struct {
	App   ApplicationConfig `mapstructure:"application"`
	Redis RedisConfig       `mapstructure:"redis"`
}

// ApplicationConfig is a struct to define configurations for the app
type ApplicationConfig struct {
	Port int `mapstructure:"port"`
}

// RedisConfig is a struct to define configurations for Redis
type RedisConfig struct {
	Host     string `mapstructure:"host"`
	Port     int    `mapstructure:"port"`
	Password string `mapstructure:"password"`
	DB       int    `mapstructure:"db"`
	Key      string `mapstructure:"key"`
}

// LoadConfig is a function to load the configuration, stored on the config files
func LoadConfig() Config {
	var config Config
	// set config file name, path and file type
	viper.AddConfigPath("./configs")
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Fail to load configs: %s", err)
	}

	if err := viper.Unmarshal(&config); err != nil {
		log.Fatalf("Unable to decode application configs: %s", err)
	}

	// Enable VIPER to read Environment Variables
	viper.AutomaticEnv()
	// assign Port for env
	port := viper.GetInt("APPLICATION_PORT")
	config.App.Port = port
	return config
}
